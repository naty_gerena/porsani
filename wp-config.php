<?php

// ATUALIZAÇÕES
define('DISALLOW_FILE_MODS', true);

// DATABASE
define('DB_HOST',         'localhost');
define('DB_NAME',         'local_assesoria');
define('DB_USER',         'root');
define('DB_PASSWORD',     '');
define('DB_TABLE_PREFIX', 'wp_vex_');

// DOMÍNIO & URL
define('PROTOCOL',        'http://');
define('DOMAIN_NAME',     'localhost:8080');
define('PATH_TO_WP',      '/porsani');





/*  ------------------------ CHAVES ÚNICAS E SALTS  --------------------- */

define('AUTH_KEY',         '.`OJQ8e/UKv.>1x&yhP2+^?tIIF73,crvVh8|jI,Bhea5$WS[^5hKX-~MLqSJqX`');
define('SECURE_AUTH_KEY',  'k$lZ=?+n9k0Ln+etB0Lw-S$IIa3Q+(`nI<Y-vf|1q`StSLQ jBLNc!wm}P=sVTp@');
define('LOGGED_IN_KEY',    'w?crU(C|yr14h=v5Z`$Hfr3&fzFs2yRwc1q+A!-NS gy;mBW&#R0^Zga&Tro,E)-');
define('NONCE_KEY',        '6bx0~j0yLrxed8#8<3bSZK3#z-57RI[ifr&r-dk#jySFF,9~AI4?S+o@| Y@?S)A');
define('AUTH_SALT',        'V15i6:-+`hUK3]klf|?n`|=LH-.n-to0of]v+ 4PY80s*J+|6<Tp] +5jF)SpVV ');
define('SECURE_AUTH_SALT', 'O?I7c&P/E6~Hw85~vIntPKFD+.kpf%t!$M5RzUw#HFySv{AbWTuoA||_h];-wx+Q');
define('LOGGED_IN_SALT',   'V{dmUyhKBnS_U%.#y>.$|^KrL|*0Bu>J.OGDt)tx^|w;j(1H: ;r{)m1b>{lUz5v');
define('NONCE_SALT',       'gv`5gL>M{.GReM1-C#!//Th9|l.87f3zgUEwG$$31BC^`=,Q%/xQt<LV=>HY1fPc');






/*  ------------------------ OUTRAS CONSTANTES  --------------------- */

define('WP_SITEURL',                 PROTOCOL . DOMAIN_NAME . PATH_TO_WP);
define('WP_HOME',                    WP_SITEURL);
define('AUTOMATIC_UPDATER_DISABLED', true);
define('WP_POST_REVISIONS',          false );
define('FS_METHOD',                  'direct');
define('WPLANG',                     'pt_BR');
define('DB_CHARSET',                 'utf8');
define('DB_COLLATE',                 '');
define('WPCF7_AUTOP',                false);

// SSL
if (PROTOCOL === 'https://'){
    define( 'FORCE_SSL_LOGIN', true );
    define( 'FORCE_SSL_ADMIN', true );
}





/*  -------------------------- ISTO É TUDO, PODE PARAR DE EDITAR  --------------------- */
$table_prefix = DB_TABLE_PREFIX;
setlocale(LC_ALL, WPLANG);
if ( !defined('ABSPATH') ) define('ABSPATH', dirname(__FILE__) . '/');
require_once(ABSPATH . 'wp-settings.php');





/*  -------------------------- WP_HEAD CLEAN UP  --------------------- */
add_filter('the_generator',      '__return_false');
add_filter('show_admin_bar',     '__return_false');

remove_action('wp_head',         'wlwmanifest_link');
remove_action('wp_head',         'rsd_link');
remove_action('wp_head',         'wp_shortlink_wp_head');
remove_action('wp_head',         'index_rel_link');
remove_action('wp_head',         'start_post_rel_link', 10, 0);
remove_action('wp_head',         'parent_post_rel_link', 10, 0);
remove_action('wp_head',         'adjacent_posts_rel_link', 10, 0);
remove_action('wp_head',         'adjacent_posts_rel_link_wp_head', 10);
remove_action('wp_head',         'feed_links', 2);
remove_action('wp_head',         'feed_links_extra', 3);
remove_action('wp_head',         'print_emoji_detection_script', 7 );
remove_action('wp_print_styles', 'print_emoji_styles' );