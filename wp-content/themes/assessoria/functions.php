<?php

// Includes -------------------------------------------------------------------
include_once "functions-modules.php";
include_once "functions-cpt.php";

// Remove Scripts 
function remove_head_scripts() { 
   remove_action('wp_head', 'wp_print_scripts'); 
   remove_action('wp_head', 'wp_print_head_scripts', 9); 
   remove_action('wp_head', 'wp_enqueue_scripts', 1);
   
 
   add_action('wp_footer', 'wp_print_scripts', 5);
   add_action('wp_footer', 'wp_enqueue_scripts', 5);
   add_action('wp_footer', 'wp_print_head_scripts', 5); 
} 

add_action( 'wp_enqueue_scripts', 'remove_head_scripts' );

function my_deregister_scripts(){
    wp_deregister_script( 'wp-embed' );
}
add_action( 'init', 'my_deregister_scripts' );

// adicionando apiSettings WPCF7
add_action( 'wp_footer', 'wp_vex_enqueue_scripts' );
function wp_vex_enqueue_scripts() {
    $wpcf7 = array(
        'apiSettings' => array(
            'root' => esc_url_raw( rest_url() ).'contact-form-7/v1',
            'namespace' => 'contact-form-7/v1',
        ),
        'recaptcha' => array(
            'messages' => array(
                'empty' =>
                    __( 'Please verify that you are not a robot.', 'contact-form-7' ),
            ),
        ),
    );

    if ( defined( 'WP_CACHE' ) && WP_CACHE ) {
        $wpcf7['cached'] = 1;
    }

    echo '<script>var wpcf7 = ' . json_encode($wpcf7) . ';</script>';
}

// Incluo algumas informações no contexto do Timber
add_filter('timber_context', 'add_to_context');
function add_to_context($data) {

    // Paginas
    $data['home']       = Timber::get_post(2);
    $data['quem_somos'] = Timber::get_post(24);
    $data['coaches']    = Timber::get_post(25);
    $data['galeria']    = Timber::get_post(26);
    $data['contato']    = Timber::get_post(27);

    return $data;
}

// Limita os caracteres do the_excerpt() --------------------------------------
function excerpt($texto, $limite, $quebra = true) {
    $texto   = strip_tags($texto);
    $tamanho = strlen($texto);
    if($tamanho <= $limite) :
        $novo_texto = $texto;
    else :
        if($quebra == true) :
            $novo_texto = trim(substr($texto, 0, $limite))."...";
        else :
            $ultimo_espaco = strrpos(substr($texto, 0, $limite), " ");
            $novo_texto = trim(substr($texto, 0, $ultimo_espaco))."...";
        endif;
    endif;
    return $novo_texto;
}



// Funcao que checa a se o link da pagina esta ativo --------------------------
function is_active($pag) {
    $slug = get_page_class();

    if ( is_array($pag) ) :

        foreach ($pag as $p) :

            if ( $slug == $p ) return "ativo";

        endforeach;

    else :

        if ( $slug == $pag ) return "ativo";

    endif;

}



// Funcao para retornar slug ou tipo de post ----------------------------------
function get_page_class()
{

    global $wp_query;
    $query = $wp_query->query;
    $slug  = get_post( $post )->post_name;

    if (is_singular('post')):
        $return = 'single';
    else :
        if ( ! empty( $query['post_type'] ) ) :
            $return = $query['post_type'];
        else:
            $return = $slug;
        endif;
    endif;

    return $return;

}
