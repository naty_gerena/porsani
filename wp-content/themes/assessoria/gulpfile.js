var elixir = require('laravel-elixir');

elixir.config.sourcemaps = true;
elixir.config.assetsPath = 'src';
elixir.config.publicPath = 'assets';

elixir(function(mix) {

    mix.sass([
        'main.scss',
    ],  'src/css/build.css');

    mix.styles([
        '../../../../plugins/contact-form-7/includes/css/styles.css',
        'libs/animate.min.css',
        'libs/magnific-popup.css',
        'libs/owl.carousel.min.css',
        'build.css',
    ],  'assets/css/app.css');

    mix.scripts([
        'libs/jquery.min.js',
        'libs/jquery.maskedinput.min.js',
        '../../../../plugins/contact-form-7/includes/js/scripts.js',
        'libs/imagesloaded.pkgd.min.js',
        'libs/jquery.scrollspy.js',
        'libs/animatescroll.min.js',
        'libs/jquery.magnific-popup.min.js',
        'libs/owl.carousel.min.js',
        'libs/wow.min.js',
        'main.js',
    ],  'assets/js/app.js');

    mix.copy('src/images', 'assets/images');
    mix.copy('src/fonts', 'assets/fonts');

});