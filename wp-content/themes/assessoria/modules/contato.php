<?php


// Defino onde vou exibir este campo
$location = setLocation(array(
    ['page' => 27],
));


// THATS IT! ------------------------------------------------------------------
if(function_exists("register_field_group"))
{
    register_field_group(array (
        'id' => 'acf_contato',
        'title' => 'Contato',
        'fields' => array (
            array (
                'key' => 'field_telefones',
                'label' => 'Telefones',
                'name' => 'telefones',
                'type' => 'repeater',
                'sub_fields' => array (
                    array (
                        'key' => 'field_telefone',
                        'label' => 'Telefone',
                        'name' => 'telefone',
                        'type' => 'text',
                        'formatting' => 'none',
                    ),
                    array (
                        'key' => 'field_whatasapp',
                        'label' => '',
                        'name' => 'whatsapp',
                        'type' => 'checkbox',
                        'choices' => array (
                            'whatsapp' => 'Whatsapp',
                        ),
                        'default_value' => '',
                        'layout' => 'vertical',
                    ),
                ),
                'layout' => 'row',
                'button_label' => 'Adicionar',
            ),
            array (
                'key' => 'field_email',
                'label' => 'Email',
                'name' => 'email',
                'type' => 'text',
                'formatting' => 'none',
            ),
            array (
                'key' => 'field_redes_sociais',
                'label' => 'Redes sociais',
                'name' => 'redes_sociais',
                'type' => 'repeater',
                'sub_fields' => array (
                    array (
                        'key' => 'field_nome',
                        'label' => 'Nome',
                        'name' => 'nome',
                        'type' => 'select',
                        'choices' => array (
                            'facebook' => 'Facebook',
                            'instagram' => 'Instagram',
                            'twitter' => 'Twitter',
                            'gplus' => 'Google+',
                            'linkedin' => 'Linkedin',
                            'youtube' => 'Youtube',
                        ),
                        'default_value' => '',
                        'allow_null' => 0,
                        'multiple' => 0,
                    ),
                    array (
                        'key' => 'field_link',
                        'label' => 'Link',
                        'name' => 'link',
                        'type' => 'text',
                        'required' => 1,
                        'formatting' => 'none',
                    ),
                ),
                'layout' => 'row',
                'button_label' => 'Adicionar',
            ),
            array (
                'key' => 'field_endereco',
                'label' => 'Endereço',
                'name' => 'endereco',
                'type' => 'textarea',
                'formatting' => 'br',
            ),
            array (
                'key' => 'field_mapa',
                'label' => 'mapa',
                'name' => 'mapa',
                'type' => 'google_map',
            ),
        ),
        
        'location' => $location,

        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
                0 => 'the_content',
            ),
        ),
        'menu_order' => 0,

    ));
}

