/***
 * FUNCTIONS
 ***/
// Check mobile
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

// ScrollAnim
function scrollAnim (target) {
    $(target).animatescroll({
        scrollSpeed: 800,
        easing: 'easeOutExpo',
        padding: 100,
    });
}

/***
 * GERAL
 ***/
$(function() {

    // menu
    var $win = $(window),
        $menu = $('header nav'),
        $menuToggle = $('header .menuToggle');

    $($menuToggle).click(function(event) {
        event.preventDefault();
        $menu.slideToggle();
        $('html').toggleClass('open-menu');
    });

    if ($win.width() <= 768) {
        $('header nav a').click(function(event) {
            $menu.slideToggle();
            $('html').toggleClass('open-menu');
        });
    }

    $win.resize(function(event) {
        $('html').removeClass('open-menu');

        if ($win.width() <= 768) {
            $($menu).css('display', 'none');
        } else {
            $($menu).css('display', 'inline-block');
        }
    });

    var header = $('header .header-topo').height();

    // $(window).scroll(function () {
    //     if ($(window).scrollTop() > header) {
    //         $('body').css('padding-top', header);
    //     }
    // });

    // imagesLoaded
    $('body').imagesLoaded().always( function( instance ) {
        $('body').addClass('loaded');
    });

    // wow
    new WOW({
        offset: 200
    }).init();


    // owl carousel
    $('.owl-carousel').owlCarousel({
        items           : 1,
        loop            : true,
        autoplay        : true,
        dots            : true,
        nav             : false
    });

    // Galeria
    $('.Galeria').magnificPopup({
        delegate: 'a',
        type: 'image',
        gallery: {
        enabled: true
      }
    });
    

    // Scrollspy
    $('body').scrollspy({
        target: 'header nav',
        offset: 150
    });

    // Scroll
    $('.scroll').on('click', function(event) {
        var data   = $(this).data('target');
        var href   = $(this).attr('href');
        var target = (data !== undefined) ? data : href;

        scrollAnim(target);
    });

    // Input masks
    $("input.cpf").mask("999.999.999-99",{autoclear: false});
    $("input.cnpj").mask("99.999.999/9999-99",{autoclear: false});
    $("input.data").mask("99/99/9999",{autoclear: false});
    $("input.cep").mask("99999-999",{autoclear: false});
    $("input.hora").mask("99:99",{autoclear: false});
    $("input.telefone").focusout(function(){
        var phone, element;
        element = $(this);
        element.unmask();
        phone = element.val().replace(/\D/g, '');
        if(phone.length > 10) {
            element.mask("(99) 9 9999-999?9",{autoclear: false});
        } else {
            element.mask("(99) 9999-9999?9",{autoclear: false});
        }
    }).trigger('focusout');

    $('form').attr('autocomplete', 'off');

});